<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function contactPost(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
        ]);

        Mail::send('email', [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'chat_message' => $request->get('message')],
            function ($email_message) {
                $email_message->from('noreply@miclv.com');
                $email_message->to('miclv.cs@gmail.com', 'Michael Vierling')
                    ->subject('Your Website Contact Form');
            });

        return back()->with('success', 'Thanks for contacting me, I will get back to you soon!');
    }
}
