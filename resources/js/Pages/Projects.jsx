import PortfolioLayout from "@/Layouts/PortfolioLayout.jsx";
import {Card, CardBody, Typography} from "@material-tailwind/react";
import mvSitePhoto from "../../images/mvsite.png"


const Projects = () => {
    return (
        <div className="flex flex-col">
            <Card
                className="m-5 p-5 w-auto bg-[#f5f5f5] shadow-md shadow-red-500 hover:shadow-red-700 hover:shadow-lg transition duration-500 hover:scale-110">
                <div
                    className="flex justify-center items-center relative rounded-xl shadow-md h-28 lg:h-44 w-full bg-gray-500/50">
                    <Typography variant="lead" color="blue-gray">Coming Soon</Typography>
                </div>
                <CardBody>
                    <Typography variant="h5" color="blue-gray" className="mb-2">
                        Upcoming Project
                    </Typography>
                </CardBody>
            </Card>
            <Card
                className="m-5 p-5 w-auto bg-[#f5f5f5] shadow-md shadow-red-500 hover:shadow-red-700 hover:shadow-lg transition duration-500 hover:scale-110">
                <img src={mvSitePhoto} alt="Photo of michaelvierling.com"
                     className="rounded-xl shadow-md w-full"/>
                <CardBody>
                    <Typography variant="h5" color="blue-gray" className="mb-2">
                        michaelvierling.com
                    </Typography>
                    <div className="grid grid-cols-2">
                        <Typography variant="small" color="gray">- Lavarel</Typography>
                        <Typography variant="small" color="gray">- Inertia.js</Typography>
                        <Typography variant="small" color="gray">- React.js</Typography>
                        <Typography variant="small" color="gray">- Vite.js</Typography>
                        <Typography variant="small" color="gray">- Tailwind</Typography>
                        <Typography variant="small" color="gray">- AWS</Typography>
                    </div>
                </CardBody>
            </Card>
        </div>
    )
}

Projects.layout = projects => <PortfolioLayout children={projects}/>

export default Projects;
