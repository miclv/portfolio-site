import PortfolioLayout from "@/Layouts/PortfolioLayout.jsx";
import {
    Certifications,
    ContactForm,
    Education,
    Experience,
    SiteInfo,
    TechnicalSkills
} from "@/Components/About/Index.js";


const About = () => {
    return (
        <>
            <div className="flex flex-col">
                <Education/>
                <Experience/>
                <TechnicalSkills/>
                <Certifications/>
                <ContactForm/>
                <SiteInfo/>
            </div>
        </>
    )
}

About.layout = home => <PortfolioLayout children={home}/>

export default About;
