import PortfolioLayout from "@/Layouts/PortfolioLayout.jsx";
import {Card, CardBody, CardFooter, Typography} from "@material-tailwind/react";

const Blog = (props) => {
    const listBlogs = props.blog_posts.map((blog) =>
        <Card shadow={true}
              className="m-5 p-5 w-auto bg-[#f5f5f5] shadow-md shadow-red-500 hover:shadow-red-700 hover:shadow-lg transition duration-500 hover:scale-105">
            <CardBody>
                <Typography variant="h5" color="black">
                    {blog.title}
                </Typography>
                <Typography variant="small" color="gray" className="mb-4">
                    {blog.date}
                </Typography>
                <Typography variant="paragraph" color="blue-gray">
                    {blog.body}
                </Typography>
            </CardBody>
            <CardFooter>
                <Typography variant="paragraph" color="red" className="text-right">
                    <span className="flex inline-flex items-center">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                             stroke="currentColor" className="w-5 h-5 mr-1 inline-block">
                          <path strokeLinecap="round" strokeLinejoin="round"
                                d="M6.75 7.5l3 2.25-3 2.25m4.5 0h3m-9 8.25h13.5A2.25 2.25 0 0021 18V6a2.25 2.25 0 00-2.25-2.25H5.25A2.25 2.25 0 003 6v12a2.25 2.25 0 002.25 2.25z"/>
                        </svg>
                        Michael
                    </span>
                </Typography>
            </CardFooter>
        </Card>
    );

    return (
        <div className="flex flex-col">
            {listBlogs}
        </div>
    )
}

Blog.layout = blog => <PortfolioLayout children={blog}/>

export default Blog;
