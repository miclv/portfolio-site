import {Intro, Navbar, Socials} from '@/Components/Header/Index.jsx';
import favicon from '/public/favicon.ico';
import {Head} from '@inertiajs/react'

const PortfolioLayout = ({children}) => {
    return (
        <>
            <Head>
                <link rel='icon' href={favicon}/>
            </Head>
            <div
                className="mx-auto min-h-screen max-w-screen-xl px-6 py-12 font-sans md:px-12 md:py-20 lg:px-24 lg:py-0">
                <div className="lg:flex lg:justify-between lg:gap-4">
                    <header
                        className="lg:sticky lg:top-0 lg:h-screen lg:flex lg:max-h-screen lg:w-1/2 lg:flex-col lg:justify-between lg:py-24">
                        <div>
                            <Intro/>
                            <Navbar/>
                        </div>
                        <Socials/>
                    </header>

                    <main className="pt-24 lg:w-1/2 lg:py-24">
                        <div>
                            {children}
                        </div>
                    </main>
                </div>
            </div>
        </>
    )
}

export default PortfolioLayout;
