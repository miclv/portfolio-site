import {Typography} from "@material-tailwind/react";
import {usePage} from "@inertiajs/react";


export default function Intro() {
    const {component} = usePage()
    return (
        <>
            <Typography variant="h1" color="white" className="text-4xl sm:text-5xl font-bold">
                Michael Vierling
            </Typography>
            <Typography variant="h3" color="red" className="mt-1 text-2xl sm:text-3xl font-bold">
                Software Developer
            </Typography>
            <Typography variant="paragraph" color="white"
                        className={component === 'About' ? 'mt-4 max-w-xs' : "hidden"}>
                I'm a diligent & driven software developer. Passionate about all things tech.
            </Typography>
            <Typography variant="paragraph" color="white"
                        className={component === 'Blog' ? 'mt-4 max-w-xs' : "hidden"}>
                Bite-sized blog posts on programming, software development & my projects.
            </Typography>
            <Typography variant="paragraph" color="white"
                        className={component === 'Projects' ? 'mt-4 max-w-xs' : "hidden"}>
                My projects in chronological order, including the tools & technologies used.
            </Typography>
        </>
    )
}
