import {Link, usePage} from '@inertiajs/react';

export default function Navbar() {

    const {component} = usePage()

    return (
        <nav>
            <ul className="lg:mt-16 mt-6 w-max">
                <li className="my-2 group">
                    <Link
                        href="/about"
                        className={component === 'About' ? 'font-bold text-red-500 text-3xl duration-150' : 'text-white text-2xl group-hover:font-bold  duration-150'}
                    >
                            <span className="flex inline-flex items-center">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     strokeWidth={1.5} stroke="currentColor"
                                     className={component === 'About' ? 'w-5 h-5 mr-2 animate-spin-slow inline-block' :
                                         'w-5 h-5 -ml-7 mr-2 invisible duration-150 group-hover:visible group-hover:ml-0 inline-block'}>
                                  <path strokeLinecap="round" strokeLinejoin="round"
                                        d="M21 7.5l-2.25-1.313M21 7.5v2.25m0-2.25l-2.25 1.313M3 7.5l2.25-1.313M3 7.5l2.25 1.313M3 7.5v2.25m9 3l2.25-1.313M12 12.75l-2.25-1.313M12 12.75V15m0 6.75l2.25-1.313M12 21.75V19.5m0 2.25l-2.25-1.313m0-16.875L12 2.25l2.25 1.313M21 14.25v2.25l-2.25 1.313m-13.5 0L3 16.5v-2.25"/>
                                </svg>
                                About
                            </span>
                    </Link>
                </li>
                <li className="my-2 group">
                    <Link
                        href="/blog"
                        className={component === 'Blog' ? 'font-bold text-red-500 text-3xl duration-150' : 'text-white text-2xl group-hover:font-bold duration-150 '}
                    >
                            <span className="flex inline-flex items-center">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     strokeWidth={1.5} stroke="currentColor"
                                     className={component === 'Blog' ? 'w-5 h-5 mr-2 animate-spin-slow inline-block' :
                                         'w-5 h-5 -ml-7 mr-2 invisible duration-150 group-hover:visible group-hover:ml-0 inline-block'}>
                                  <path strokeLinecap="round" strokeLinejoin="round"
                                        d="M21 7.5l-2.25-1.313M21 7.5v2.25m0-2.25l-2.25 1.313M3 7.5l2.25-1.313M3 7.5l2.25 1.313M3 7.5v2.25m9 3l2.25-1.313M12 12.75l-2.25-1.313M12 12.75V15m0 6.75l2.25-1.313M12 21.75V19.5m0 2.25l-2.25-1.313m0-16.875L12 2.25l2.25 1.313M21 14.25v2.25l-2.25 1.313m-13.5 0L3 16.5v-2.25"/>
                                </svg>
                                Blog
                            </span>
                    </Link>
                </li>
                <li className="my-2 group">
                    <Link
                        href="/projects"
                        className={component === 'Projects' ? 'font-bold text-red-500 text-3xl duration-150' : 'text-white text-2xl group-hover:font-bold duration-150'}
                    >
                            <span className="flex inline-flex items-center">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     strokeWidth={1.5} stroke="currentColor"
                                     className={component === 'Projects' ? 'w-5 h-5 mr-2 animate-spin-slow inline-block' :
                                         'w-5 h-5 -ml-7 mr-2 invisible duration-150 group-hover:visible group-hover:ml-0 inline-block'}>
                                  <path strokeLinecap="round" strokeLinejoin="round"
                                        d="M21 7.5l-2.25-1.313M21 7.5v2.25m0-2.25l-2.25 1.313M3 7.5l2.25-1.313M3 7.5l2.25 1.313M3 7.5v2.25m9 3l2.25-1.313M12 12.75l-2.25-1.313M12 12.75V15m0 6.75l2.25-1.313M12 21.75V19.5m0 2.25l-2.25-1.313m0-16.875L12 2.25l2.25 1.313M21 14.25v2.25l-2.25 1.313m-13.5 0L3 16.5v-2.25"/>
                                </svg>
                                Projects
                            </span>
                    </Link>
                </li>
            </ul>
        </nav>
    )
}
