import Intro from './Intro.jsx'
import Navbar from './Navbar.jsx'
import Socials from './Socials.jsx'

export {Intro, Navbar, Socials}
