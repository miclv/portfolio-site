import React from "react";
import {Card, Tab, TabPanel, Tabs, TabsBody, TabsHeader, Typography} from "@material-tailwind/react";
import phpLogo from "../../../images/php.svg";
import jsLogo from "../../../images/javascript.svg"
import pythonLogo from "../../../images/python.svg";
import htmlLogo from "../../../images/html.svg";
import cssLogo from "../../../images/css.svg";
import laravelLogo from "../../../images/laravel.svg";
import reactLogo from "../../../images/react.svg";
import tailwindLogo from "../../../images/tailwind.svg";
import djangoLogo from "../../../images/django.svg";
import wpLogo from "../../../images/wordpress.svg";
import gitLogo from "../../../images/git.svg";
import linuxLogo from "../../../images/linux.svg";
import dockerLogo from "../../../images/docker.svg";
import mysqlLogo from "../../../images/mysql.svg";
import awsLogo from "../../../images/aws.svg";


export default function TechnicalSkills() {
    const data = [
        {
            label: "Programming Languages",
            value: "programming-languages",
            desc:
                <div className="grid grid-cols-2 sm:grid-cols-3">
                    <div
                        className="flex-col container mx-auto group hover:scale-125 duration-300">
                        <img className="mx-auto object-contain h-6" src={phpLogo} alt="PHP Logo"/>
                        <Typography variant="paragraph" color="blue-gray" className="text-center mt-2 mb-4  sm:mb-2">
                            PHP
                        </Typography>
                    </div>
                    <div className="flex-col container mx-auto group hover:scale-125 duration-300">
                        <img className="mx-auto object-contain h-6 text-red-500" src={jsLogo} alt="JavaScript Logo"/>

                        <Typography variant="paragraph" color="blue-gray" className="text-center mt-2 mb-4  sm:mb-2">
                            JavaScript
                        </Typography>
                    </div>
                    <div className="flex-col container mx-auto group hover:scale-125 duration-300">
                        <img className="mx-auto object-contain h-6" src={pythonLogo} alt="Python Logo"/>
                        <Typography variant="paragraph" color="blue-gray" className="text-center mt-2 mb-4 sm:mb-2">
                            Python
                        </Typography>
                    </div>
                    <div className="flex-col container mx-auto group hover:scale-125 duration-300">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor"
                             className="mx-auto object-contain h-6 sm:mt-4 text-indigo-900">
                            <path fillRule="evenodd"
                                  d="M10 1c3.866 0 7 1.79 7 4s-3.134 4-7 4-7-1.79-7-4 3.134-4 7-4zm5.694 8.13c.464-.264.91-.583 1.306-.952V10c0 2.21-3.134 4-7 4s-7-1.79-7-4V8.178c.396.37.842.688 1.306.953C5.838 10.006 7.854 10.5 10 10.5s4.162-.494 5.694-1.37zM3 13.179V15c0 2.21 3.134 4 7 4s7-1.79 7-4v-1.822c-.396.37-.842.688-1.306.953-1.532.875-3.548 1.369-5.694 1.369s-4.162-.494-5.694-1.37A7.009 7.009 0 013 13.179z"
                                  clipRule="evenodd"/>
                        </svg>
                        <Typography variant="paragraph" color="blue-gray" className="text-center mt-2 mb-4 sm:mb-0">
                            SQL
                        </Typography>
                    </div>
                    <div className="flex-col container mx-auto group hover:scale-125 duration-300">
                        <img className="mx-auto object-contain h-6 sm:mt-4" src={htmlLogo} alt="HTML Logo"/>
                        <Typography variant="paragraph" color="blue-gray" className="text-center mt-2">
                            HTML
                        </Typography>
                    </div>
                    <div className="flex-col container mx-auto group hover:scale-125 duration-300">
                        <img className="mx-auto object-contain h-6 sm:mt-4" src={cssLogo} alt="CSS Logo"/>
                        <Typography variant="paragraph" color="blue-gray" className="text-center mt-2">
                            CSS
                        </Typography>
                    </div>
                </div>
        },
        {
            label: "Libraries & Frameworks",
            value: "libraries-frameworks",
            desc:
                <div className="grid grid grid-cols-2 sm:grid-cols-4">
                    <div className="flex-col container mx-auto group hover:scale-125 duration-300">
                        <img className="mx-auto object-contain h-6" src={laravelLogo} alt="Laravel Logo"/>
                        <Typography variant="paragraph" color="blue-gray" className="text-center mt-2 mb-4 sm:mb-0">
                            Laravel
                        </Typography>
                    </div>
                    <div className="flex-col container mx-auto group hover:scale-125 duration-300">
                        <img className="mx-auto object-contain h-6" src={reactLogo} alt="React Logo"/>
                        <Typography variant="paragraph" color="blue-gray" className="text-center mt-2 mb-4 sm:mb-0">
                            React.js
                        </Typography>
                    </div>
                    <div className="flex-col container mx-auto group hover:scale-125 duration-300">
                        <img className="mx-auto object-contain h-6" src={tailwindLogo} alt="Tailwind Logo"/>
                        <Typography variant="paragraph" color="blue-gray" className="text-center mt-2">
                            Tailwind
                        </Typography>
                    </div>

                    <div className="flex-col container mx-auto group hover:scale-125 duration-300">
                        <img className="mx-auto object-contain h-6" src={djangoLogo} alt="Django Logo"/>
                        <Typography variant="paragraph" color="blue-gray" className="text-center mt-2">
                            Django
                        </Typography>
                    </div>
                </div>
        },
        {
            label: "Tools & Platforms",
            value: "tools-platforms",
            desc:
                <div className="grid grid-cols-2 sm:grid-cols-3">
                    <div className="flex-col container mx-auto group hover:scale-125 duration-300">
                        <img className="mx-auto object-contain h-6" src={wpLogo} alt="WordPress Logo"/>
                        <Typography variant="paragraph" color="blue-gray" className="text-center mt-2 mb-4 sm:mb-2">
                            WordPress
                        </Typography>
                    </div>
                    <div className="flex-col container mx-auto group hover:scale-125 duration-300">
                        <img className="mx-auto object-contain h-6" src={gitLogo} alt="Git Logo"/>
                        <Typography variant="paragraph" color="blue-gray" className="text-center mt-2 mb-4 sm:mb-2">
                            Git
                        </Typography>
                    </div>
                    <div className="flex-col container mx-auto group hover:scale-125 duration-300">
                        <img className="mx-auto object-contain h-6" src={linuxLogo} alt="Linux Logo"/>
                        <Typography variant="paragraph" color="blue-gray" className="text-center mt-2 mb-4 sm:mb-2">
                            Linux
                        </Typography>
                    </div>
                    <div className="flex-col container mx-auto group hover:scale-125 duration-300">
                        <img className="mx-auto object-contain h-6 sm:mt-4" src={dockerLogo} alt="Docker Logo"/>
                        <Typography variant="paragraph" color="blue-gray" className="text-center mt-2 mb-4 sm:mb-0">
                            Docker
                        </Typography>
                    </div>
                    <div className="flex-col container mx-auto group hover:scale-125 duration-300">
                        <img className="mx-auto object-contain h-6 sm:mt-4" src={mysqlLogo} alt="MySQL Logo"/>
                        <Typography variant="paragraph" color="blue-gray" className="text-center mt-2">
                            MySQL
                        </Typography>
                    </div>
                    <div className="flex-col container mx-auto group hover:scale-125 duration-300">
                        <img className="mx-auto object-contain h-6 sm:mt-4" src={awsLogo} alt="Vite Logo"/>
                        <Typography variant="paragraph" color="blue-gray" className="text-center mt-2">
                            AWS
                        </Typography>
                    </div>
                </div>
        },
    ];
    return (
        <Card shadow={true}
              className="m-5 p-5 bg-[#f5f5f5] w-auto shadow-md shadow-red-500 hover:shadow-red-700 hover:shadow-lg transition duration-500 hover:scale-110">
            <Typography variant="h2" color="black" className="mb-1">Technical Skills</Typography>
            <Tabs value="programming-languages" className="w-auto">
                <TabsHeader
                    className="bg-transparent flex-col items-start"
                    indicatorProps={{
                        className: "bg-red-500/30 shadow-none",
                    }}
                >
                    {data.map(({label, value}) => (
                        <Tab
                            className="w-full text-[#263238] text-md md:text-lg !justify-normal duration-300 hover:pl-4"
                            key={value}
                            value={value}>
                            {label}
                        </Tab>
                    ))}
                </TabsHeader>
                <TabsBody animate={{
                    initial: {y: 100},
                    mount: {y: 0},
                    unmount: {y: 100},
                }}>
                    {data.map(({value, desc}) => (
                        <TabPanel key={value} value={value}>
                            {desc}
                        </TabPanel>
                    ))}
                </TabsBody>
            </Tabs>
        </Card>
    )
}
