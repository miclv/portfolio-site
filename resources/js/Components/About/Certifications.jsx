import {Card, Typography} from "@material-tailwind/react";

export default function Certifications() {
    return (
        <Card shadow={true}
              className="m-5 p-5 w-auto bg-[#f5f5f5] shadow-md shadow-red-500 hover:shadow-red-700 hover:shadow-lg transition duration-500 hover:scale-110">

            <Typography variant="h2" color="black" className="mb-1">Certifications</Typography>
            <Typography color="blue-gray" className="text-lg sm:text-xl">
                <a className="flex inline-flex items-baseline group duration-300 hover:text-red-500"
                   href="https://www.credly.com/badges/36fe5b56-d86b-4a9a-90b6-8e549b16cae5" target="_blank"
                   rel="noreferrer">
                    <span>Cisco Certified DevNet Associate
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                         strokeWidth={2} stroke="currentColor"
                         className="w-4 h-4 ml-1 mb-1 inline-block duration-150 group-hover:-translate-y-1">
                        <path strokeLinecap="round" strokeLinejoin="round"
                              d="M13.5 6H5.25A2.25 2.25 0 003 8.25v10.5A2.25 2.25 0 005.25 21h10.5A2.25 2.25 0 0018 18.75V10.5m-10.5 6L21 3m0 0h-5.25M21 3v5.25"/>
                    </svg>
                    </span>
                </a>
            </Typography>
            <Typography color="blue-gray" className="text-lg sm:text-xl">
                <a className="flex inline-flex items-baseline group duration-300 hover:text-red-500"
                   href="https://www.credly.com/badges/06b5eece-cde9-40b7-a0e2-8b593f0ed390" target="_blank"
                   rel="noreferrer">
                    <span>Cisco Meraki Solutions Specialist
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                         strokeWidth={2} stroke="currentColor"
                         className="w-4 h-4 ml-1 mb-1 inline-block duration-150 group-hover:-translate-y-1">
                        <path strokeLinecap="round" strokeLinejoin="round"
                              d="M13.5 6H5.25A2.25 2.25 0 003 8.25v10.5A2.25 2.25 0 005.25 21h10.5A2.25 2.25 0 0018 18.75V10.5m-10.5 6L21 3m0 0h-5.25M21 3v5.25"/>
                    </svg>
                    </span>
                </a>
            </Typography>
        </Card>
    )
}
