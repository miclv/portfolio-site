import Certifications from "./Certifications.jsx";
import ContactForm from "./ContactForm.jsx";
import Education from "./Education.jsx";
import Experience from "./Experience.jsx";
import TechnicalSkills from "./TechnicalSkills.jsx";
import SiteInfo from "./SiteInfo.jsx";


export {Certifications, ContactForm, Education, Experience, TechnicalSkills, SiteInfo}
