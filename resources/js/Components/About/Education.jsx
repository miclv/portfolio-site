import msumDragon from '../../../images/msum-dragon.svg';
import {Card, Typography} from "@material-tailwind/react";

export default function Education() {
    return (
        <Card shadow={true} id="education"
              className="m-5 p-5 w-auto bg-[#f5f5f5] shadow-md shadow-red-500 hover:shadow-red-700 hover:shadow-lg transition duration-500 hover:scale-110">
            <div className="flex inline-flex items-baseline justify-between mb-1">
                <Typography variant="h2" color="black">Education</Typography>
                <img className="h-10 w-auto" src={msumDragon} alt="MSUM Logo"/>
            </div>

            <Typography variant="h5" color="blue-gray">
                <a className="flex inline-flex items-baseline group duration-300 hover:text-red-500"
                   href="https://www.mnstate.edu/" target="_blank"
                   rel="noreferrer">
                        <span>Minnesota State University Moorhead
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                             strokeWidth={2} stroke="currentColor"
                             className="w-4 h-4 ml-1 mb-1 inline-block duration-150 group-hover:-translate-y-1.5">
                            <path strokeLinecap="round" strokeLinejoin="round"
                                  d="M13.5 6H5.25A2.25 2.25 0 003 8.25v10.5A2.25 2.25 0 005.25 21h10.5A2.25 2.25 0 0018 18.75V10.5m-10.5 6L21 3m0 0h-5.25M21 3v5.25"/>
                        </svg>
                        </span>

                </a>
            </Typography>
            <Typography variant="small" color="gray">May 2021</Typography>
            <Typography variant="paragraph" color="blue-gray">
                B.S. in <span className="text-red-500">Computer Science</span>
            </Typography>
            <Typography variant="paragraph" color="blue-gray">
                B.S. in <span className="text-red-500">Computer Information Technology</span>
            </Typography>
        </Card>
    )
}
