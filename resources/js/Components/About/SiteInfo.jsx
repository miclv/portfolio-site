import {Card, Tooltip, Typography} from "@material-tailwind/react";

export default function SiteInfo() {
    const scrollToTop = () => {
        window.scrollTo({top: 0, left: 0, behavior: 'smooth'});
    };

    return (
        <Card color="transparent"
              className="m-5 p-5 w-auto border-2 border-red-500 shadow-md shadow-red-500 hover:shadow-red-700 hover:shadow-lg transition duration-500 hover:scale-110">
            <div className="mb-4 text-red-500 w-full flex justify-center">
                <Tooltip content="Return to Top" animate={{
                    mount: {scale: 1, y: 0},
                    unmount: {scale: 0, y: 25},
                }}>
                    <a onClick={scrollToTop}>
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                             stroke="currentColor"
                             className="w-8 h-8 duration-300 hover:-translate-y-1.5">
                            <path strokeLinecap="round" strokeLinejoin="round"
                                  d="M4.5 12.75l7.5-7.5 7.5 7.5m-15 6l7.5-7.5 7.5 7.5"/>
                        </svg>
                    </a>
                </Tooltip>
            </div>
            <Typography variant="paragraph" color="red">
                Made with <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor"
                                         className="w-5 h-5 inline-block mr-1 text-red-900 animate-pulse">
                                      <path
                                          d="M11.645 20.91l-.007-.003-.022-.012a15.247 15.247 0 01-.383-.218 25.18 25.18 0 01-4.244-3.17C4.688 15.36 2.25 12.174 2.25 8.25 2.25 5.322 4.714 3 7.688 3A5.5 5.5 0 0112 5.052 5.5 5.5 0 0116.313 3c2.973 0 5.437 2.322 5.437 5.25 0 3.925-2.438 7.111-4.739 9.256a25.175 25.175 0 01-4.244 3.17 15.247 15.247 0 01-.383.219l-.022.012-.007.004-.003.001a.752.752 0 01-.704 0l-.003-.001z"/>
                                    </svg>
                            </span>
                using <a href="https://laravel.com/" target="_blank" rel="noreferrer"
                         className="font-bold hover:text-white duration-150">Laravel</a>,
                <a href="https://react.dev/" target="_blank" rel="noreferrer"
                   className="font-bold hover:text-white duration-150"> React.js</a> &
                <a href="https://tailwindcss.com/" target="_blank" rel="noreferrer"
                   className="font-bold hover:text-white duration-150"> Tailwind</a>. Deployed on
                <a href="https://aws.amazon.com/" target="_blank" rel="noreferrer"
                   className="font-bold hover:text-white duration-150"> AWS</a> using
                <a href="https://forge.laravel.com/" target="_blank" rel="noreferrer"
                   className="font-bold hover:text-white duration-150"> Forge</a>.
            </Typography>
        </Card>
    )
}
