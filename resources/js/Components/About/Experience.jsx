import {
    Card,
    Timeline,
    TimelineBody,
    TimelineConnector,
    TimelineHeader,
    TimelineIcon,
    TimelineItem,
    Typography,
} from "@material-tailwind/react";

export default function Experience() {
    return (
        <Card shadow={true}
              className="m-5 p-5 w-auto bg-[#f5f5f5] shadow-md shadow-red-500 hover:shadow-red-700 hover:shadow-lg transition duration-500 hover:scale-110">
            <Typography variant="h2" color="black" className="mb-3">Experience</Typography>

            <Timeline>
                <TimelineItem>
                    <TimelineConnector/>
                    <TimelineHeader className="h-3">
                        <TimelineIcon/>
                        <Typography variant="h5" color="black" className="leading-none">
                            Application Developer
                        </Typography>
                    </TimelineHeader>
                    <TimelineBody className="pb-8">
                        <Typography variant="paragraph" color="blue-gray">
                            <a className="flex inline-flex items-baseline group font-bold duration-300 hover:text-red-500"
                               href="https://www.netcenter.net/"
                               target="_blank"
                               rel="noreferrer">
                                <span>Network Center Inc - Remote
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     strokeWidth={2} stroke="currentColor"
                                     className="w-4 h-4 ml-1 mb-1 inline-block duration-150 group-hover:-translate-y-1">
                                    <path strokeLinecap="round" strokeLinejoin="round"
                                          d="M13.5 6H5.25A2.25 2.25 0 003 8.25v10.5A2.25 2.25 0 005.25 21h10.5A2.25 2.25 0 0018 18.75V10.5m-10.5 6L21 3m0 0h-5.25M21 3v5.25"/>
                                </svg>
                                </span>
                            </a>
                        </Typography>
                        <Typography variant="small" color="gray">January 2023 - July 2023</Typography>

                        <ul className="flex flex-wrap">
                            <li className="mr-1.5 mt-2">
                                <div
                                    className="flex items-center rounded-full bg-red-300/10 px-3 py-1 text-xs font-medium leading-5 text-red-500">
                                    WordPress
                                </div>
                            </li>
                            <li className="mr-1.5 mt-2">
                                <div
                                    className="flex items-center rounded-full bg-red-300/10 px-3 py-1 text-xs font-medium leading-5 text-red-500">
                                    JavaScript
                                </div>
                            </li>
                            <li className="mr-1.5 mt-2">
                                <div
                                    className="flex items-center rounded-full bg-red-300/10 px-3 py-1 text-xs font-medium leading-5 text-red-500">
                                    PHP
                                </div>
                            </li>
                            <li className="mr-1.5 mt-2">
                                <div
                                    className="flex items-center rounded-full bg-red-300/10 px-3 py-1 text-xs font-medium leading-5 text-red-500">
                                    HTML
                                </div>
                            </li>
                            <li className="mr-1.5 mt-2">
                                <div
                                    className="flex items-center rounded-full bg-red-300/10 px-3 py-1 text-xs font-medium leading-5 text-red-500">
                                    CSS
                                </div>
                            </li>
                            <li className="mr-1.5 mt-2">
                                <div
                                    className="flex items-center rounded-full bg-red-300/10 px-3 py-1 text-xs font-medium leading-5 text-red-500">
                                    Git
                                </div>
                            </li>
                            <li className="mr-1.5 mt-2">
                                <div
                                    className="flex items-center rounded-full bg-red-300/10 px-3 py-1 text-xs font-medium leading-5 text-red-500">
                                    GitLab
                                </div>
                            </li>
                        </ul>
                    </TimelineBody>
                </TimelineItem>
                <TimelineItem>
                    <TimelineConnector/>
                    <TimelineHeader className="h-3">
                        <TimelineIcon/>
                        <Typography variant="h5" color="black" className="leading-none">
                            IT Field Technician
                        </Typography>
                    </TimelineHeader>
                    <TimelineBody className="pb-2">
                        <Typography variant="paragraph" color="blue-gray">
                            <a className="flex inline-flex items-baseline group font-bold duration-300 hover:text-red-500"
                               href="https://www.procellis.com/"
                               target="_blank"
                               rel="noreferrer">
                                <span>Procellis Technology Inc
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     strokeWidth={2} stroke="currentColor"
                                     className="w-4 h-4 ml-1 mb-1 duration-150 inline-block group-hover:-translate-y-1">
                                    <path strokeLinecap="round" strokeLinejoin="round"
                                          d="M13.5 6H5.25A2.25 2.25 0 003 8.25v10.5A2.25 2.25 0 005.25 21h10.5A2.25 2.25 0 0018 18.75V10.5m-10.5 6L21 3m0 0h-5.25M21 3v5.25"/>
                                </svg>
                                </span>
                            </a>
                        </Typography>
                        <Typography variant="small" color="gray">July 2021 - January 2023</Typography>
                        <ul className="flex flex-wrap">
                            <li className="mr-1.5 mt-2">
                                <div
                                    className="flex items-center rounded-full bg-red-300/10 px-3 py-1 text-xs font-medium leading-5 text-red-500">
                                    Computer Networking
                                </div>
                            </li>
                            <li className="mr-1.5 mt-2">
                                <div
                                    className="flex items-center rounded-full bg-red-300/10 px-3 py-1 text-xs font-medium leading-5 text-red-500">
                                    Python
                                </div>
                            </li>
                        </ul>
                    </TimelineBody>
                </TimelineItem>
            </Timeline>
        </Card>
    )
}
