import {useForm, usePage} from '@inertiajs/react';
import {Button, Card, Input, Textarea, Typography} from "@material-tailwind/react";

export default function ContactForm(props) {
    const {data, setData, post, reset, processing, errors} = useForm({
        name: '',
        email: '',
        message: '',
    })

    const {flash} = usePage().props;

    function submit(e) {
        e.preventDefault()
        post('/contact', {
            preserveScroll: true,
            onSuccess: () => reset('name', 'email', 'message')
        })
    }

    return (
        <Card shadow={true}
              className="m-5 p-5 w-auto bg-[#f5f5f5] shadow-md shadow-red-500 hover:shadow-red-700 hover:shadow-lg transition duration-500 hover:scale-110">
            <form onSubmit={submit}>
                <div>
                    <Typography variant="h2" color="black" className="mb-3">
                        Contact Me
                    </Typography>
                    <div className="mb-4 flex flex-col gap-1">
                        <Input
                            error={errors.name}
                            variant="outlined"
                            label="Name"
                            type="text"
                            value={data.name}
                            onChange={e => setData("name", e.target.value)}
                            size="md"
                            className="focus:ring-0"

                        />
                        {errors.name && <Typography variant="small" color="red">{errors.name}</Typography>}
                    </div>
                    <div className="mb-4 flex flex-col gap-1">
                        <Input
                            error={errors.email}
                            label="Email"
                            type="email"
                            value={data.email}
                            onChange={e => setData("email", e.target.value)}
                            size="md"
                            className="focus:ring-0"
                        />
                        {errors.email && <Typography variant="small" color="red">{errors.email}</Typography>}
                    </div>
                    <div className="mb-4 flex flex-col">
                        <Textarea
                            error={errors.message}
                            color="gray"
                            label="Message"
                            value={data.message}
                            onChange={e => setData("message", e.target.value)}
                            rows={3}
                            className="focus:ring-0"
                        />
                        {errors.message && <Typography variant="small" color="red">{errors.message}</Typography>}
                        {flash.success && <Typography variant="small" color="green">{flash.success}</Typography>}
                    </div>
                    <div className="block mb-4">
                        <Button
                            color="red"
                            size="md"
                            type="submit"
                            disabled={processing}
                            className="duration-150 hover:bg-red-900"
                        >
                            Submit
                        </Button>
                    </div>
                </div>
            </form>
        </Card>
    )
}
