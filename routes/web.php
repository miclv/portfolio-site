<?php

use App\Http\Controllers\ContactController;
use App\Models\BlogPost;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return Inertia::render('About');
});

Route::get('/about', function () {
    return Inertia::render('About');
});

Route::get('/blog', function () {
    return Inertia::render('Blog', [
        'blog_posts' => BlogPost::all()->map(fn ($blog_post) => [
            'title' => $blog_post->title,
            'date' => $blog_post->date,
            'body' => $blog_post->body,
        ]),
    ]);
});

Route::get('/projects', function () {
    return Inertia::render('Projects');
});

Route::post('/contact', [ContactController::class, 'contactPost'])->name('contactPost');

require __DIR__.'/auth.php';
